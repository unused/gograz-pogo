// Package powercacher contains a demonstrational calculation cache that is
// using redis db. As the cache itself is a thousand times slower to handle
// than calculating a power result, yet this method is fine for a demo ;)
package powercacher

import (
	"context"
	"fmt"
	"math"

	"github.com/go-redis/redis/v8"
)

// Cacher provides a redis cached power calculator.
type Cacher struct {
	db  *redis.Client
	ctx context.Context
}

// New provides a new cacher using a local redis connection and the default
// database.
func New() *Cacher {
	db := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		DB:       0,
		Password: "",
	})
	ctx := context.Background()
	return &Cacher{db, ctx}
}

// Calc returns a (cached) power result for the given values or creates a new
// cache entry. Note that retrieving the cache does take much longer than
// calculating the result... but we need some DB for this demonstrational
// example and therefor it totally makes sense to cache the result.
func (c *Cacher) Calc(x, y int) (int, error) {
	key := cacheKey(x, y)
	val, err := c.db.Get(c.ctx, key).Int()
	if err == nil {
		return val, nil
	}
	if err != redis.Nil {
		return 0, err
	}
	res := int(math.Pow(float64(x), float64(y)))
	c.cache(key, res)
	return res, nil
}

// cache does cache a result.
func (c *Cacher) cache(key string, val int) error {
	return c.db.Set(c.ctx, key, val, 0).Err()
}

// cacheKey calculates a cache key from two given input values.
func cacheKey(x, y int) string {
	return fmt.Sprintf("%d^%d", x, y)
}
