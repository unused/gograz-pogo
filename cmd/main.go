package main

import (
	"fmt"
	"os"
	"powercacher"
	"strconv"
)

func main() {
	if len(os.Args) != 3 {
		fmt.Println("Usage: \n  $ powercacher 42 7")
		os.Exit(1)
	}

	x, err := strconv.Atoi(os.Args[1])
	if err != nil {
		panic(err)
	}
	y, err := strconv.Atoi(os.Args[2])
	if err != nil {
		panic(err)
	}

	pow := powercacher.New()
	z, err := pow.Calc(x, y)
	if err != nil {
		panic(err)
	}
	fmt.Printf("  %d^%d = %d\n", x, y, z)
}
