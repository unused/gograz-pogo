---
title: Pogo Demo for Go Graz Meetup 03-2022
author: Christoph Lipautz
theme: Copenhagen
colortheme: beaver
date: \today
---

# Overview

- Elements of GitLab Continuous Integration
- Setup Pogo and a Go Project
- Outlook and Discussion

# Goals of Tool "Pogo"

A GitLab CI custom executor using podman. Exceptionally easy to setup, very
easy, adaptable and versatile to use.

- https://github.com/eyeson-team/pogo

# What is GitLab?

GitLab open source alternative to GitHub (Bitbucket, etc). Popular at
open source enthusiasts and companies that strive to keep (and manage) their
software products in-house.

- https://about.gitlab.com/

# Continuous Integration (and Deployment)

> Who did break the testsuite?

# GitLab CI


A project (git repository) stores an instruction recipe in a `.gitlab-ci.yml`
file orchestrating jobs (tasks, actions) per pipeline (push event).

Commonly we run: static code analyzers, testsuites, coverage reports,
(scheduled) security audits, builds, deployments.

# GitLab CI Runner

Manages job processing on a machine. ...and it is written in Go.

- https://gitlab.com/gitlab-org/gitlab-runner

# Podman

Containers are valuable tools when used properly. Podman is a great alternative
to Docker, highly compatible and runs on user-level. ...and it is written in
Go.

- https://podman.io/
- https://github.com/containers/podman

# GitLab CI Executors

We have to decide in which environment our tasks are executed.

- https://docs.gitlab.com/runner/executors/#selecting-the-executor

# Sidenote: CI Specs

It will run in our infrastructure, has access to our source code. If our
deployments are triggered by CI jobs it even has access to crucial
infrastructure.

- https://stackoverflow.blog/2021/01/25/a-deeper-dive-into-our-may-2019-security-incident/

# Executors Compared

- Shell (or SSH) requires lots of fiddling, think about databases, cleanups,
  etc. May require complex custom rules and structure.
- Virtualbox or Parallels... Hell no!
- Kubernetes!? Have a team of devops to run and maintain you cluster to run CI
  jobs.
- Docker sounds ok, but it runs as a deamon and has su rights.

# Pogo To The Rescue

- Showcase: Download, Setup Runner, Create a Sample Project
- Includes: Service Image, Parallel Execution, Artifacts

# Processing Steps

- config ... configures gitlab-runner. (Unlikely to fail)
- prepare ... start services and container, copy essential files
  (gitlab-runner) and install git (and ca-certificates). (Must not fail)
- run ... process the job (provided as a script file by the runner), is
  expected to succeed or fail, both is good.
- cleanup ... remove pods (container)

# Networking with Services

After several different strategies: Attach services to the job container
network, allowing "local" access.

> --net container:id: Reuse another container's network stack.

- https://podman.io/getting-started/network
- https://docs.podman.io/en/latest/markdown/podman-run.1.html#network-mode-net

# Internals

Very simple wrapper that aims to be easy to use but powerful through
configuration. Utilizes Cobra (cli framework) and Viper (configuration
management framework).

- https://github.com/spf13/cobra
- https://github.com/spf13/viper

# Straightforward Podman Commands

It executes sh commands `$ podman`, but would be really nice to have built-in
podman and gitlab-runner as well :)

However the downside of this is a limit to features available and requires
specific podman and gitlab-runner versions to use (fear of missing out new
features!).

# GitLab Env & API

Heavily relies on GitLab CI Environment variables and API (for a limited amount
of information).

# Pogo Configuration

- Mount stuff from the host machine (e.g. dependency cache, large test files,
  etc.)
- Add podman arguments (build a container using builder, e.g. https://developers.redhat.com/blog/2019/08/14/best-practices-for-running-buildah-in-a-container)
- https://github.com/eyeson-team/pogo#install--register

# Future Work

- Prepare registration call to avoid tedious arguments.
- Avoid or replace install script.
- GitHub action to provide multi-platform binaries.
- Example and best practice collection.

