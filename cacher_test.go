package powercacher

import (
	"math"
	"testing"
)

func TestPowerResult(t *testing.T) {
	pow := New()
	ans, err := pow.Calc(8, 42)
	if err != nil {
		t.Errorf("Calc failed with %v", err)
	}

	exp := int(math.Pow(8.0, 42.0))
	if ans != exp {
		t.Errorf("8^42 = %d, want %d", ans, exp)
	}

	cacheAns := int(math.Pow(8.0, 42.0))
	if ans != cacheAns {
		t.Errorf("cached result = %d, want %d", ans, exp)
	}
}
