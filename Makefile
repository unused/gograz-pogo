BUILD_OPTS=-t beamer

presentation.pdf: presentation.md
	pandoc $(BUILD_OPTS) -o $@ $<

.PHONY: watch
watch:
	ls *.md | entr -s make

.PHONY: install
install:
	./bin/gitlab-runner register \
		--url $(GITLAB_URL) \
		--name "$(HOSTNAME) runner" \
		--executor custom \
		--shell bash \
		--builds-dir /builds \
		--cache-dir /cache \
		--custom-config-exec "$(PWD)/bin/pogo" \
		--custom-config-args "config" \
		--custom-config-args "--config" \
		--custom-config-args "$(PWD)/.pogo.yaml" \
		--custom-prepare-exec "$(PWD)/bin/pogo" \
		--custom-prepare-args "prepare" \
		--custom-prepare-args "--config" \
		--custom-prepare-args "$(PWD)/.pogo.yaml" \
		--custom-run-exec "$(PWD)/bin/pogo" \
		--custom-run-args "run" \
		--custom-run-args "--config" \
		--custom-run-args "$(PWD)/.pogo.yaml" \
		--custom-cleanup-exec "$(PWD)/bin/pogo" \
		--custom-cleanup-args "clean" \
		--custom-cleanup-args "--config" \
		--custom-cleanup-args "$(PWD)/.pogo.yaml"

.PHONY: run
run:
	./bin/gitlab-runner run --working-directory $(PWD)
