
# Pogo Demo

A short presentation and demo of pogoa for gograz meetup.

## Demo App

- Create a new gitlab project.
- Deactivate shared runner.
- Download
  [gitlab-runner](https://docs.gitlab.com/runner/install/linux-manually.html#using-binary-file)
  and [pogo](https://github.com/eyeson-team/pogo/releases/tag/v0.1.0) and
  [install script](https://raw.githubusercontent.com/eyeson-team/pogo/main/bin/install-packages)
- Setup a runner with custom executor pogo.
- Push the sample project:

```sh
$ go mod init gitlab.com/unused/powercacher
$ cp presentation/*.go .
$ vim .gitlab-ci.yml
$ curl -L --output ./setup/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
$ curl -L --output ./setup/pogo "https://github.com/eyeson-team/pogo/releases/download/v0.1.0/pogo"
$ chmod u+x setup/*
$ make install GITLAB_URL=https://gitlab.com
$ vim ~/.gitlab-runner/config.toml # increase concurrent val
$ make run
```
